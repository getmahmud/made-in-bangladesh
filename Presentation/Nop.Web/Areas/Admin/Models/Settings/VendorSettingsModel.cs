﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;

namespace Nop.Web.Areas.Admin.Models.Settings
{
    public partial class VendorSettingsModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.VendorsBlockItemsToDisplay")]
        public int VendorsBlockItemsToDisplay { get; set; }
        public bool VendorsBlockItemsToDisplay_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.ShowVendorOnProductDetailsPage")]
        public bool ShowVendorOnProductDetailsPage { get; set; }
        public bool ShowVendorOnProductDetailsPage_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.AllowCustomersToContactVendors")]
        public bool AllowCustomersToContactVendors { get; set; }
        public bool AllowCustomersToContactVendors_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.AllowCustomersToApplyForVendorAccount")]
        public bool AllowCustomersToApplyForVendorAccount { get; set; }
        public bool AllowCustomersToApplyForVendorAccount_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.TermsOfServiceEnabled")]
        public bool TermsOfServiceEnabled { get; set; }
        public bool TermsOfServiceEnabled_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.AllowSearchByVendor")]
        public bool AllowSearchByVendor { get; set; }
        public bool AllowSearchByVendor_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.AllowVendorsToEditInfo")]
        public bool AllowVendorsToEditInfo { get; set; }
        public bool AllowVendorsToEditInfo_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.NotifyStoreOwnerAboutVendorInformationChange")]
        public bool NotifyStoreOwnerAboutVendorInformationChange { get; set; }
        public bool NotifyStoreOwnerAboutVendorInformationChange_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.MaximumProductNumber")]
        public int MaximumProductNumber { get; set; }
        public bool MaximumProductNumber_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.MaximumProductNumberForFreeMember")]
        public int MaximumProductNumberForFreeMember { get; set; }
        public bool MaximumProductNumberForFreeMember_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.MaximumProductNumberForBasic")]
        public int MaximumProductNumberForBasic { get; set; }
        public bool MaximumProductNumberForBasic_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.AllowVendorsToImportProducts")]
        public bool AllowVendorsToImportProducts { get; set; }
        public bool AllowVendorsToImportProducts_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.MaximumProductPostingForStandard")]
        public int MaximumProductPostingForStandard { get; set; }
        public bool MaximumProductPostingForStandard_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.MaximumProductPostingForPremium")]
        public int MaximumProductPostingForPremium { get; set; }
        public bool MaximumProductPostingForPremium_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.MaximumProductShowcaseNumberForFreeMember")]
        public int MaximumProductShowcaseNumberForFreeMember { get; set; }
        public bool MaximumProductShowcaseNumberForFreeMember_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.MaximumProductShowcaseNumberForBasic")]
        public int MaximumProductShowcaseNumberForBasic { get; set; }
        public bool MaximumProductShowcaseNumberForBasic_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.MaximumProductShowcaseForStandard")]
        public int MaximumProductShowcaseForStandard { get; set; }
        public bool MaximumProductShowcaseForStandard_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Vendor.MaximumProductShowcaseForPremium")]
        public int MaximumProductShowcaseForPremium { get; set; }
        public bool MaximumProductShowcaseForPremium_OverrideForStore { get; set; }
    }
}