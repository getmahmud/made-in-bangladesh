﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Services.Customers;
using Nop.Services.Suppliers;
using Nop.Services.Vendors;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Web.Models.Common;

namespace Nop.Web.Components
{
    public class CompanyHeaderViewComponent : NopViewComponent
    {
        private readonly IVendorService _vendorService;
        private readonly ICustomerService _customerService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        public CompanyHeaderViewComponent(IHttpContextAccessor httpContextAccessor,
            IVendorService vendorService, ICustomerService customerService)
        {
            this._httpContextAccessor = httpContextAccessor;
            this._vendorService = vendorService;
            this._customerService = customerService;
        }

        public IViewComponentResult Invoke()
        {
            var model = new CompanyHeaderModel();

            var currentSupplierId = 0;
            if (!string.IsNullOrEmpty(_session.GetString("SupplierId")))
            {
                currentSupplierId = int.Parse(_session.GetString("SupplierId"));
            }
            if(currentSupplierId > 0)
            {
                var currentSupplier = _customerService.GetCustomerById(currentSupplierId);
                var companyName = _vendorService.GetVendorById(currentSupplier.VendorId).Name;
                model.Id = currentSupplierId;
                model.Name = companyName;
            }            

            return View(model);
        }
    }
}
