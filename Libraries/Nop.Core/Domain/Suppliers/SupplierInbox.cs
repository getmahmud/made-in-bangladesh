﻿using System;

namespace Nop.Core.Domain.Suppliers
{
    /// <summary>
    /// Represents SupplierInbox entity
    /// </summary>
    public partial class SupplierInbox : BaseEntity
    {       
        /// <summary>
        /// Gets or sets the supplier id
        /// </summary>
        public int SupplierId { get; set; }

        /// <summary>
        /// Gets or sets the buyer id
        /// </summary>
        public int BuyerId { get; set; }

        /// <summary>
        /// Gets or sets the product id
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets product quantity
        /// </summary>
        public int ProductQuantity { get; set; }

        /// <summary>
        /// Gets or sets the quantity unit
        /// </summary>
        public int ProductQuantityUnit { get; set; }

        /// <summary>
        /// Gets or sets the message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the reply
        /// </summary>
        public string Reply { get; set; }

        /// <summary>
        /// Gets or sets the date and time of creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of update
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }
    }
}
