namespace Nop.Core.Domain.Customers
{
    /// <summary>
    /// Represents the user account type formatting enumeration
    /// </summary>
    public enum UserAccountType
    {
        /// <summary>
        /// Buyer
        /// </summary>
        Buyer = 1,

        /// <summary>
        /// Supplier
        /// </summary>
        Supplier = 2,

        /// <summary>
        /// Supplier
        /// </summary>
        Admin = 2
    }
}
