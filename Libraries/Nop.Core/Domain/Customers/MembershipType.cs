namespace Nop.Core.Domain.Customers
{
    /// <summary>
    /// Represents the membership type formatting enumeration
    /// </summary>
    public enum MembershipType
    {
        /// <summary>
        /// Free
        /// </summary>
        Free = 1,

        /// <summary>
        /// Paid
        /// </summary>
        Paid = 2
    }
}
