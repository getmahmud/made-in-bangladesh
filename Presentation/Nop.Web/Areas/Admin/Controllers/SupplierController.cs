﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Suppliers;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.QuantityUnits;
using Nop.Services.Security;
using Nop.Services.Suppliers;
using Nop.Web.Areas.Admin.Models.Suppliers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Models.Supplier;
using System;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class SupplierController : BaseAdminController
    {
        #region Fields
        private readonly IWorkContext _workContext;
        private readonly ISupplierService _supplierService;
        private readonly IProductService _productService;
        private readonly ICustomerService _customerService;
        private readonly IQuantityUnitService _quantityUnitService;
        private readonly IPermissionService _permissionService;
        private readonly IDateTimeHelper _dateTimeHelper;

        #endregion

        #region Ctor

        public SupplierController(
            IWorkContext workContext, 
            ISupplierService supplierService,
            IProductService productService, 
            ICustomerService customerService,
            IQuantityUnitService quantityUnitService,
            IPermissionService permissionService,
            IDateTimeHelper dateTimeHelper)
        {
            this._workContext = workContext;
            this._supplierService = supplierService;
            this._productService = productService;
            this._customerService = customerService;
            this._quantityUnitService = quantityUnitService;
            this._permissionService = permissionService;
            this._dateTimeHelper = dateTimeHelper;
        }
        
        #endregion
        
        #region Utilities
        
        public virtual IActionResult SupplierInfo()
        {
            var model = new SupplierInfoModel();
            var supplierInfo = _supplierService.GetSupplierInfoBySupplierId(_workContext.CurrentCustomer.Id);

            if(supplierInfo != null)
            {
                model.Id = supplierInfo.Id;
                model.SupplierId = supplierInfo.SupplierId;
                model.AboutUs = supplierInfo.AboutUs;
                model.ContactUS = supplierInfo.ContactUS;
            }
            else
            {
                model.SupplierId = _workContext.CurrentCustomer.Id;
            }
            
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult SupplierInfo(SupplierInfoModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                var supplierInfo = new SupplierInfo()
                {
                    Id = model.Id,
                    SupplierId = model.SupplierId,
                    AboutUs = model.AboutUs,
                    ContactUS = model.ContactUS
                };

                if (supplierInfo.Id > 0)
                {
                    _supplierService.UpdateSupplierInfo(supplierInfo);
                }
                else
                {
                    _supplierService.InsertSupplierInfo(supplierInfo);
                }
            }

            return RedirectToAction("SupplierInfo", "Supplier", new { area = "admin" });
        }

        public virtual IActionResult Inbox()
        {
            var model = new SupplierInboxListModel();
            
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Inbox(DataSourceRequest command, SupplierInboxListModel model)
        {
            var supplierInboxes = _supplierService.GetSupplierInboxListBySupplierId(_workContext.CurrentCustomer.Id);
            if (supplierInboxes != null && supplierInboxes.Count > 0)
            {
                foreach (var item in supplierInboxes)
                {
                    var product = _productService.GetProductById(item.ProductId);
                    var buyer = _customerService.GetCustomerById(item.BuyerId);
                    var quantityUnit = _quantityUnitService.GetQuantityUnitById(item.ProductQuantityUnit).Name;

                    var supplierInbox = new Models.Suppliers.SupplierInboxModel()
                    {
                        Id = item.Id,
                        ProductId = product.Id,
                        ProductName = product.Name,
                        BuyerId = buyer.Id,
                        BuyerEmail = buyer.Email,
                        ProductQuantity = item.ProductQuantity,
                        QuantityUnit = quantityUnit,
                        Message = item.Message,
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(item.CreatedOnUtc, DateTimeKind.Utc),
                        UpdatedOn = _dateTimeHelper.ConvertToUserTime(item.UpdatedOnUtc, DateTimeKind.Utc)
                };
                    model.SupplierInboxList.Add(supplierInbox);
                }
            }

            var gridModel = new DataSourceResult
            {
                Data = model.SupplierInboxList
            };

            return Json(gridModel);
        }

        [HttpPost]
        public virtual IActionResult InboxDelete(int id)
        {
            var inbox = _supplierService.GetSupplierInboxById(id);
            if (inbox == null)
                throw new ArgumentException("No inbox item found with the specified id");

            if (inbox.SupplierId != _workContext.CurrentCustomer.Id)
                return Content("This is not your inbox item");

            _supplierService.DeleteSupplierInbox(inbox);

            return new NullJsonResult();
        }
        
        public virtual IActionResult Reply(int id)
        {
            var model = new Models.Suppliers.SupplierInboxModel();
            var inbox = _supplierService.GetSupplierInboxById(id);
            if(inbox != null)
            {
                model.Id = inbox.Id;
                model.SupplierId = inbox.SupplierId;
                model.BuyerId = inbox.BuyerId;
                var buyer = _customerService.GetCustomerById(inbox.BuyerId);
                if(buyer != null)
                {
                    model.BuyerEmail = buyer.Email;
                }
                var product = _productService.GetProductById(inbox.ProductId);
                if(product != null)
                {
                    model.ProductName = product.Name;
                }
                model.ProductQuantity = inbox.ProductQuantity;
                model.ProductQuantityUnit = inbox.ProductQuantityUnit;
                var quantityUnit = _quantityUnitService.GetQuantityUnitById(inbox.ProductQuantityUnit);
                if(quantityUnit != null)
                {
                    model.QuantityUnit = quantityUnit.Name;
                }
                model.Message = inbox.Message;
                model.CreatedOn = _dateTimeHelper.ConvertToUserTime(inbox.CreatedOnUtc, DateTimeKind.Utc);
                model.UpdatedOn = _dateTimeHelper.ConvertToUserTime(inbox.UpdatedOnUtc, DateTimeKind.Utc);
                model.Reply = inbox.Reply;
            }

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Reply(Models.Suppliers.SupplierInboxModel model)
        {
            if (ModelState.IsValid)
            {
                var supplierInbox = _supplierService.GetSupplierInboxById(model.Id);
                supplierInbox.Reply = model.Reply;

                _supplierService.UpdateSupplierInbox(supplierInbox);
            }

            return RedirectToAction("Inbox", "Supplier", new { area = "admin" });
        }

        #endregion

    }
}