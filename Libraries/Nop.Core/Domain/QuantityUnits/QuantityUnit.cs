﻿using System;

namespace Nop.Core.Domain.QuantityUnits
{
    /// <summary>
    /// Represents QuantityUnit entity
    /// </summary>
    public partial class QuantityUnit : BaseEntity
    {       
        /// <summary>
        /// Gets or sets the quantity unit Name
        /// </summary>
        public string Name { get; set; }
    }
}
