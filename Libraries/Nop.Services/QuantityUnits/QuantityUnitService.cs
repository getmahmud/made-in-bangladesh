using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.News;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Polls;
using Nop.Core.Domain.QuantityUnits;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Suppliers;
using Nop.Core.Extensions;
using Nop.Data;
using Nop.Services.Common;
using Nop.Services.Events;
using Nop.Services.Suppliers;

namespace Nop.Services.QuantityUnits
{
    /// <summary>
    /// Supplier service
    /// </summary>
    public partial class QuantityUnitService : IQuantityUnitService
    {
        #region Fields
        private readonly IRepository<QuantityUnit> _quantityUnitRepository;
        #endregion

        #region Ctor
        public QuantityUnitService(IRepository<QuantityUnit> quantityUnitRepository)
        {
            this._quantityUnitRepository = quantityUnitRepository;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets a quantity unit
        /// </summary>
        /// <param name="id">QuantityUnit identifier</param>
        /// <returns>A single QuantityUnit</returns>
        public virtual QuantityUnit GetQuantityUnitById(int id)
        {
            if (id == 0)
                return null;

            return _quantityUnitRepository.GetById(id);
        }

        /// <summary>
        /// Gets all quantity units
        /// </summary>
        /// <returns>All QuantityUnits</returns>
        public virtual IList<QuantityUnit> GetAllQuantityUnits()
        {
            return _quantityUnitRepository.Table.ToList();
        }

        #endregion
    }
}