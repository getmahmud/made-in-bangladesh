using Nop.Core.Domain.Suppliers;

namespace Nop.Data.Mapping.Suppliers
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class SupplierInboxMap : NopEntityTypeConfiguration<SupplierInbox>
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public SupplierInboxMap()
        {
            this.ToTable("SupplierInbox");
        }
    }
}