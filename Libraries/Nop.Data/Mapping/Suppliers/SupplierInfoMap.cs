using Nop.Core.Domain.Suppliers;

namespace Nop.Data.Mapping.Suppliers
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class SupplierInfoMap : NopEntityTypeConfiguration<SupplierInfo>
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public SupplierInfoMap()
        {
            this.ToTable("SupplierInfo");
            this.HasKey(s => s.Id);
        }
    }
}