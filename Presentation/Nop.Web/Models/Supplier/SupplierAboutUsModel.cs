﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Models.Catalog;
using Nop.Web.Validators.Supplier;

namespace Nop.Web.Models.Supplier
{
    public partial class SupplierAboutUsModel : BaseNopEntityModel
    {
        public SupplierAboutUsModel()
        {
            
        }
        
        public string AboutUs { get; set; }
    }
}