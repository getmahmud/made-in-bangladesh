﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Validators.Suppliers;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Validators.Supplier;

namespace Nop.Web.Areas.Admin.Models.Suppliers
{
    [Validator(typeof(SupplierInfoValidator))]
    public partial class SupplierInfoModel : BaseNopEntityModel
    {
        public int SupplierId { get; set; }
        
        public string AboutUs { get; set; }
        
        public string ContactUS { get; set; }
    }
}