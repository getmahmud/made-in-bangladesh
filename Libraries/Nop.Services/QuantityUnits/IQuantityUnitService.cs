using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.QuantityUnits;
using Nop.Core.Domain.Suppliers;

namespace Nop.Services.QuantityUnits
{
    /// <summary>
    /// QuantityUnit service interface
    /// </summary>
    public partial interface IQuantityUnitService
    {
        /// <summary>
        /// Gets a quantity unit
        /// </summary>
        /// <param name="id">QuantityUnit identifier</param>
        /// <returns>A single QuantityUnit</returns>
        QuantityUnit GetQuantityUnitById(int id);

        /// <summary>
        /// Gets all quantity units
        /// </summary>
        /// <returns>All QuantityUnits</returns>
        IList<QuantityUnit> GetAllQuantityUnits();
    }
}