﻿using System;

namespace Nop.Core.Domain.Suppliers
{
    /// <summary>
    /// Represents SupplierInfo entity
    /// </summary>
    public partial class SupplierInfo : BaseEntity
    {       
        /// <summary>
        /// Gets or sets the supplier id
        /// </summary>
        public int SupplierId { get; set; }

        /// <summary>
        /// Gets or sets supplier about us
        /// </summary>
        public string AboutUs { get; set; }

        /// <summary>
        /// Gets or sets supplier contact us
        /// </summary>
        public string ContactUS { get; set; }
    }
}
