﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Validators.Supplier;

namespace Nop.Web.Areas.Admin.Models.Suppliers
{
    public partial class SupplierInboxListModel : BaseNopEntityModel
    {
        public SupplierInboxListModel()
        {
            this.SupplierInboxList = new List<SupplierInboxModel>();
        }

        public IList<SupplierInboxModel> SupplierInboxList { get; set; }
    }
}