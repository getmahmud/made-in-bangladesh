﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;

namespace Nop.Web.Controllers
{
    public partial class HomeController : BasePublicController
    {
        private readonly IWorkContext _workContext;

        public HomeController(IWorkContext workContext)
        {
            this._workContext = workContext;
        }

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult Index()
        {

            if (_workContext.CurrentCustomer.MembershipType > 0 && _workContext.CurrentCustomer.AccountType == 2 && !_workContext.CurrentCustomer.Paid)
            {                
                return RedirectToRoute("CheckoutPaymentMethod");
            }
            return View();
        }
    }
}