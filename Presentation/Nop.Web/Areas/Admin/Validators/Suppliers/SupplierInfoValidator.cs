﻿using Nop.Web.Areas.Admin.Models.Suppliers;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Supplier;

namespace Nop.Web.Areas.Admin.Validators.Suppliers
{
    public partial class SupplierInfoValidator : BaseNopValidator<SupplierInfoModel>
    {
        public SupplierInfoValidator()
        {
            
        }
    }
}