﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Supplier;
using Nop.Web.Validators.Supplier;

namespace Nop.Web.Models.Buyer
{
    public partial class MessageListModel : BaseNopEntityModel
    {
        public MessageListModel()
        {
            this.MessageList = new List<SupplierInboxModel>();
        }
        
        public IList<SupplierInboxModel> MessageList { get; set; }
    }
}