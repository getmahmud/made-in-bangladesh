using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Suppliers;

namespace Nop.Services.Suppliers
{
    /// <summary>
    /// Supplier service interface
    /// </summary>
    public partial interface ISupplierService
    {
        /// <summary>
        /// Insert a supplier inbox
        /// </summary>
        /// <param name="supplierInbox">SupplierInbox</param>
        void InsertSupplierInbox(SupplierInbox supplierInbox);

        /// <summary>
        /// Insert a supplier info
        /// </summary>
        /// <param name="supplierInfo">SupplierInfo</param>
        void InsertSupplierInfo(SupplierInfo supplierInfo);

        /// <summary>
        /// Update a supplier info
        /// </summary>
        /// <param name="supplierInfo">SupplierInfo</param>
        void UpdateSupplierInfo(SupplierInfo supplierInfo);

        /// <summary>
        /// Update a supplier inbox
        /// </summary>
        /// <param name="supplierInbox">SupplierInbox</param>
        void UpdateSupplierInbox(SupplierInbox supplierInbox);


        /// <summary>
        /// Get a supplier info by supplier id
        /// </summary>
        /// <param name="supplierId">SupplierId</param>
        SupplierInfo GetSupplierInfoBySupplierId(int supplierId);

        /// <summary>
        /// Get a supplier inbox by id
        /// </summary>
        /// <param name="id">Id</param>
        SupplierInbox GetSupplierInboxById(int id);

        /// <summary>
        /// Get all supplier inboxes by supplierId
        /// </summary>
        /// <param name="supplierId">Supplier Id</param>
        IList<SupplierInbox> GetSupplierInboxListBySupplierId(int supplierId);

        /// <summary>
        /// Get all supplier inboxes by supplierId
        /// </summary>
        /// <param name="buyerId">Buyer Id</param>
        IList<SupplierInbox> GetSupplierInboxListByBuyerId(int buyerId);

        /// <summary>
        /// Deletes a supplier inbox
        /// </summary>
        /// <param name="supplierInbox">SupplierInbox</param>
        void DeleteSupplierInbox(SupplierInbox supplierInbox);
    }
}