﻿using Nop.Web.Framework.Mvc.Models;

namespace Nop.Web.Models.Common
{
    public partial class CompanyHeaderModel : BaseNopModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}