using Nop.Core.Domain.QuantityUnits;

namespace Nop.Data.Mapping.Suppliers
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class QuantityUnitMap : NopEntityTypeConfiguration<QuantityUnit>
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public QuantityUnitMap()
        {
            this.ToTable("QuantityUnit");
            this.HasKey(s => s.Id);
        }
    }
}