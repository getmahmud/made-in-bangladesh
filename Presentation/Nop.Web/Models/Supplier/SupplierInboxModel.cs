﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Validators.Supplier;

namespace Nop.Web.Models.Supplier
{
    [Validator(typeof(SupplierInboxValidator))]
    public partial class SupplierInboxModel : BaseNopEntityModel
    {
        public SupplierInboxModel()
        {
            this.AvailableProductQuantityUnits = new List<SelectListItem>();
        }

        public bool IsAuthenticated { get; set; }
        public Product Product { get; set; }
        public int ProductId { get; set; }
        public string ProductSeName { get; set; }
        public int ProductQuantity { get; set; }
        public int SelectedProductQuantityUnit { get; set; }
        public string SelectedQuantityUnit { get; set; }
        public int SupplierId { get; set; }
        public string Company { get; set; }
        public int BuyerId { get; set; }
        public string Message { get; set; }
        public string Reply { get; set; }
        public List<SelectListItem> AvailableProductQuantityUnits { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}