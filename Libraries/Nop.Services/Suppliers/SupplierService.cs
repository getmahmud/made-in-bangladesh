using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.News;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Polls;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Suppliers;
using Nop.Core.Extensions;
using Nop.Data;
using Nop.Services.Common;
using Nop.Services.Events;
using Nop.Services.Suppliers;

namespace Nop.Services.Suppliers
{
    /// <summary>
    /// Supplier service
    /// </summary>
    public partial class SupplierService : ISupplierService
    {
        #region Fields
        private readonly IRepository<SupplierInbox> _supplierInboxRepository;
        private readonly IRepository<SupplierInfo> _supplierInfoRepository;
        private readonly IEventPublisher _eventPublisher;
        #endregion

        #region Ctor
        public SupplierService(IRepository<SupplierInbox> supplierInboxRepository,
            IRepository<SupplierInfo> supplierInfoRepository,
            IEventPublisher eventPublisher)
        {
            this._supplierInboxRepository = supplierInboxRepository;
            this._supplierInfoRepository = supplierInfoRepository;
            this._eventPublisher = eventPublisher;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Insert a supplier inbox
        /// </summary>
        /// <param name="supplierInbox">SupplierInbox</param>
        public virtual void InsertSupplierInbox(SupplierInbox supplierInbox)
        {
            if (supplierInbox == null)
                throw new ArgumentNullException(nameof(supplierInbox));

            _supplierInboxRepository.Insert(supplierInbox);
        }

        /// <summary>
        /// Update a supplier inbox
        /// </summary>
        /// <param name="supplierInbox">SupplierInbox</param>
        public virtual void UpdateSupplierInbox(SupplierInbox supplierInbox)
        {
            if (supplierInbox == null)
                throw new ArgumentNullException(nameof(supplierInbox));

            if (supplierInbox.Id == 0)
                throw new Exception();

            _supplierInboxRepository.Update(supplierInbox);
        }


        /// <summary>
        /// Insert a supplier info
        /// </summary>
        /// <param name="supplierInfo">SupplierInfo</param>
        public virtual void InsertSupplierInfo(SupplierInfo supplierInfo)
        {
            if (supplierInfo == null)
                throw new ArgumentNullException(nameof(supplierInfo));

            _supplierInfoRepository.Insert(supplierInfo);
        }

        /// <summary>
        /// Update a supplier info
        /// </summary>
        /// <param name="supplierInfo">SupplierInfo</param>
        public virtual void UpdateSupplierInfo(SupplierInfo supplierInfo)
        {
            if (supplierInfo == null)
                throw new ArgumentNullException(nameof(supplierInfo));

            if (supplierInfo.Id == 0)
                throw new Exception();

            _supplierInfoRepository.Update(supplierInfo);
        }

        /// <summary>
        /// Get a supplier info by supplier id
        /// </summary>
        /// <param name="supplierId">SupplierId</param>
        public virtual SupplierInfo GetSupplierInfoBySupplierId(int supplierId)
        {
            if (supplierId == 0)
                return null;

            return _supplierInfoRepository.Table.Where(x => x.SupplierId == supplierId).FirstOrDefault();
        }

        /// <summary>
        /// Get a supplier inbox by id
        /// </summary>
        /// <param name="id">Id</param>
        public virtual SupplierInbox GetSupplierInboxById(int id)
        {
            if (id == 0)
                return null;

            return _supplierInboxRepository.Table.Where(x => x.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Get all supplier inboxes by supplierId
        /// </summary>
        /// <param name="supplierId">Supplier Id</param>
        public virtual IList<SupplierInbox> GetSupplierInboxListBySupplierId(int supplierId)
        {
            if (supplierId == 0)
                return null;

            return _supplierInboxRepository.Table.Where(x => x.SupplierId == supplierId).ToList();
        }

        /// <summary>
        /// Get all supplier inboxes by supplierId
        /// </summary>
        /// <param name="buyerId">Buyer Id</param>
        public virtual IList<SupplierInbox> GetSupplierInboxListByBuyerId(int buyerId)
        {
            if (buyerId == 0)
                return null;

            return _supplierInboxRepository.Table.Where(x => x.BuyerId == buyerId).ToList();
        }

        /// <summary>
        /// Deletes a supplier inbox
        /// </summary>
        /// <param name="supplierInbox">SupplierInbox</param>
        public virtual void DeleteSupplierInbox(SupplierInbox supplierInbox)
        {
            if (supplierInbox == null)
                throw new ArgumentNullException(nameof(supplierInbox));

            _supplierInboxRepository.Delete(supplierInbox);

            //event notification
            _eventPublisher.EntityDeleted(supplierInbox);
        }

        #endregion
    }
}