﻿using System;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Web.Framework.Mvc.Models;

namespace Nop.Web.Areas.Admin.Models.Suppliers
{
    public partial class SupplierInboxModel : BaseNopEntityModel
    {
        public int SupplierId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int BuyerId { get; set; }
        public string BuyerEmail { get; set; }
        public int ProductQuantity { get; set; }
        public int ProductQuantityUnit { get; set; }
        public string QuantityUnit { get; set; }
        public string Message { get; set; }
        public string Reply { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}