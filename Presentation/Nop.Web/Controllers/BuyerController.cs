﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Suppliers;
using Nop.Core.Domain.Vendors;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.QuantityUnits;
using Nop.Services.Seo;
using Nop.Services.Suppliers;
using Nop.Services.Vendors;
using Nop.Web.Factories;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Models.Buyer;
using Nop.Web.Models.Supplier;
using Nop.Web.Models.Vendors;

namespace Nop.Web.Controllers
{
    public partial class BuyerController : BasePublicController
    {
        #region Fields
        private readonly IProductService _productService;
        private readonly ICustomerService _customerService;
        private readonly IWorkContext _workContext;
        private readonly ISupplierService _supplierService;
        private readonly IVendorService _vendorService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IQuantityUnitService _quantityUnitService;
        private readonly ICategoryService _categoryService;
        private readonly IDateTimeHelper _dateTimeHelper;

        #endregion

        #region Ctor
        public BuyerController(IProductService productService,
            ICustomerService customerService, IWorkContext workContext, 
            ISupplierService supplierService, IVendorService vendorService,
            IProductModelFactory productModelFactory, IQuantityUnitService quantityUnitService,
            ICategoryService categoryService, IDateTimeHelper dateTimeHelper)
        {
            this._productService = productService;
            this._customerService = customerService;
            this._workContext = workContext;
            this._supplierService = supplierService;
            this._vendorService = vendorService;
            this._productModelFactory = productModelFactory;
            this._quantityUnitService = quantityUnitService;
            this._categoryService = categoryService;
            this._dateTimeHelper = dateTimeHelper;
        }


        #endregion

        #region Utilities



        #endregion

        #region Methods

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult ContactSupplier(int productId)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                return RedirectToRoute("Login");
            }

            var model = new SupplierInboxModel();

            var product = _productService.GetProductById(productId);
            if(product != null)
            {
                List<SelectListItem> availableProductQuantityUnits = new List<SelectListItem>();

                var quantityUnits = _quantityUnitService.GetAllQuantityUnits();
                if (quantityUnits.Count > 0)
                {
                    foreach (var item in quantityUnits)
                    {
                        SelectListItem quantityUnit = new SelectListItem()
                        {
                            Text = item.Name,
                            Value = item.Id.ToString()
                        };
                        availableProductQuantityUnits.Add(quantityUnit);
                    }

                    model.AvailableProductQuantityUnits = availableProductQuantityUnits;
                }
                var supplier = _customerService.GetCustomerByVendorId(product.VendorId);

                model.IsAuthenticated = _workContext.CurrentCustomer.IsRegistered();
                model.Product = product;
                model.ProductSeName = product.GetSeName();
                model.SupplierId = supplier.Id;
                model.Company = supplier.BillingAddress.Company;                    
            }            

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult ContactSupplier(SupplierInboxModel model)
        {
            if (ModelState.IsValid)
            {
                var supplierInbox = new SupplierInbox()
                {
                    SupplierId = model.SupplierId,
                    BuyerId = _workContext.CurrentCustomer.Id,
                    ProductId = model.ProductId,
                    ProductQuantity = model.ProductQuantity,
                    ProductQuantityUnit = model.SelectedProductQuantityUnit,
                    Message = model.Message,
                    CreatedOnUtc = DateTime.UtcNow,
                    UpdatedOnUtc = DateTime.UtcNow
                };
                _supplierService.InsertSupplierInbox(supplierInbox);

                return RedirectToRoute("ContactSupplierConfirm");
            }

            var product = _productService.GetProductById(model.ProductId);
            if (product != null)
            {
                List<SelectListItem> availableProductQuantityUnits = new List<SelectListItem>();

                var quantityUnits = _quantityUnitService.GetAllQuantityUnits();
                if (quantityUnits.Count > 0)
                {
                    foreach (var item in quantityUnits)
                    {
                        SelectListItem quantityUnit = new SelectListItem()
                        {
                            Text = item.Name,
                            Value = item.Id.ToString()
                        };
                        availableProductQuantityUnits.Add(quantityUnit);
                    }

                    model.AvailableProductQuantityUnits = availableProductQuantityUnits;
                }
                var supplier = _customerService.GetCustomerByVendorId(product.VendorId);

                model.IsAuthenticated = _workContext.CurrentCustomer.IsRegistered();
                model.Product = product;
                model.ProductSeName = product.GetSeName();
                model.SupplierId = supplier.Id;
                model.Company = supplier.BillingAddress.Company;
            }

            return View(model);
        }

        public virtual IActionResult ContactSupplierConfirm()
        {
            ViewBag.Result = "You have successfully contacted the supplier!";

            return View();
        }

        public virtual IActionResult SupplierDetails(int supplierId)
        {
            var model = new SupplierDetailsModel();
            var supplier = _customerService.GetCustomerById(supplierId);
            if(supplier != null)
            {
                var products = _productService.GetProductsByVendorId(supplier.VendorId);
                IList<int> categoryIds = new List<int>();
                if (products != null && products.Count > 0)
                {
                    model.Products = _productModelFactory.PrepareProductOverviewModels(products).ToList();

                    foreach (var item in products)
                    {
                        if (item.ProductCategories.Count > 0)
                        {
                            foreach (var category in item.ProductCategories)
                            {
                                if (!categoryIds.Contains(category.Id))
                                {
                                    var newCategory = new SupplierProductCategoryModel()
                                    {
                                        Id = category.Id,
                                        Name = category.Category.Name,
                                        SeName = category.Category.GetSeName()
                                    };
                                    model.Categories.Add(newCategory);
                                    categoryIds.Add(category.Id);
                                }
                            }
                        }
                    }
                }
            }                       

            return View(model);
        }

        public virtual IActionResult SupplierAboutUs(int supplierId)
        {
            var model = new SupplierAboutUsModel();

            var supplierInfo = _supplierService.GetSupplierInfoBySupplierId(supplierId);
            if(supplierInfo != null)
            {
                model.AboutUs = supplierInfo.AboutUs;
            }            

            return View(model);
        }

        public virtual IActionResult SupplierContactUs(int supplierId)
        {
            var model = new SupplierContactUsModel();

            var supplierInfo = _supplierService.GetSupplierInfoBySupplierId(supplierId);
            if (supplierInfo != null)
            {
                model.ContactUs = supplierInfo.ContactUS;
            }                

            return View(model);
        }

        public virtual IActionResult MessageList()
        {
            var model = new MessageListModel();

            var supplierInboxes = _supplierService.GetSupplierInboxListBySupplierId(_workContext.CurrentCustomer.Id);
            if (supplierInboxes!= null && supplierInboxes.Count > 0)
            {
                foreach (var item in supplierInboxes)
                {
                    var supplierInbox = new SupplierInboxModel();

                    supplierInbox.Id = item.Id;
                    supplierInbox.ProductQuantity = item.ProductQuantity;
                    supplierInbox.Message = item.Message;
                    var product = _productService.GetProductById(item.ProductId);
                    if(product != null)
                    {
                        supplierInbox.Product = product;
                        supplierInbox.ProductId = product.Id;
                        supplierInbox.ProductSeName = product.GetSeName();
                    }
                    var supplier = _customerService.GetCustomerById(item.SupplierId);
                    if(supplier != null)
                    {
                        supplierInbox.SupplierId = supplier.Id;
                        supplierInbox.Company = supplier.BillingAddress.Company;
                    }
                    var qtyUnit = _quantityUnitService.GetQuantityUnitById(item.ProductQuantityUnit);
                    if(qtyUnit != null)
                    {
                        supplierInbox.SelectedQuantityUnit = qtyUnit.Name;
                    }                    
                    supplierInbox.CreatedOn = _dateTimeHelper.ConvertToUserTime(item.CreatedOnUtc, DateTimeKind.Utc);
                    supplierInbox.UpdatedOn = _dateTimeHelper.ConvertToUserTime(item.UpdatedOnUtc, DateTimeKind.Utc);

                    model.MessageList.Add(supplierInbox);
                }
            }

            return View(model);
        }

        public virtual IActionResult MessageDetails(int messageId)
        {
            var model = new MessageDetailsModel();

            var messageBox = _supplierService.GetSupplierInboxById(messageId);
            if (messageBox != null)
            {
                model.Id = messageBox.Id;
                model.SupplierId = messageBox.SupplierId;
                var supplier = _customerService.GetCustomerById(messageBox.SupplierId);
                if(supplier != null)
                {
                    model.Company = supplier.BillingAddress.Company;
                }
                var product = _productService.GetProductById(messageBox.ProductId);
                if(product != null)
                {
                    model.Product = product;
                    model.ProductId = product.Id;
                    model.ProductSeName = product.GetSeName();
                }
                model.ProductQuantity = messageBox.ProductQuantity;
                model.SelectedProductQuantityUnit = messageBox.ProductQuantityUnit;
                var qtyUnit = _quantityUnitService.GetQuantityUnitById(messageBox.ProductQuantityUnit);
                if (qtyUnit != null)
                {
                    model.SelectedQuantityUnit = qtyUnit.Name;
                }
                model.Message = messageBox.Message;
                model.Reply = messageBox.Reply;
                model.CreatedOn = _dateTimeHelper.ConvertToUserTime(messageBox.CreatedOnUtc, DateTimeKind.Utc);
                model.UpdatedOn = _dateTimeHelper.ConvertToUserTime(messageBox.UpdatedOnUtc, DateTimeKind.Utc);
            }

            return View(model);
        }

        #endregion
    }
}